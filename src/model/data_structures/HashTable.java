package model.data_structures;

import java.util.ArrayList;

import model.vo.VOTrip;



public class HashTable<K,V>{

	private int m;
	private int size;
	
	 private ArrayList<HashNode<K,V>> array;
	
	public HashTable() {
		array = new ArrayList<HashNode<K,V>>();
        m = 11;
        size = 0;
 
      
        for (int i = 0; i < m; i++) {
            array.add(null);
        }		
	}
	
	
	public void put(K key, V value) {
		
        int i = getIndex(key);
        HashNode<K, V> head = array.get(i);
 
       
        while (head != null)
        {
            if (head.key.equals(key))
            {
                head.value = value;
                return;
            }
            head = head.next;
        }
 
      

        size++;
        head = array.get(i);
        HashNode<K, V> newNode = new HashNode<K, V>(key, value);
        newNode.next = head;
        array.set(i, newNode);
 
        
        if ((1.0*size)/m >= 0.7)
        {
            ArrayList<HashNode<K, V>> temp = array;
            array = new ArrayList<>();
            m = 2 * m;
            size = 0;
            for (int j = 0; j < m; j++)
                array.add(null);
 
            for (HashNode<K, V> headNode : temp)
            {
                while (headNode != null)
                {
                    put(headNode.key, headNode.value);
                    headNode = headNode.next;
                }
            }
        }
		
	}
	
	public  V  push(K key) 
	{
		 
        int i = getIndex(key);
        HashNode<K,V> head = array.get(i);
 
        
        while (head != null)
        {
            if (head.key.equals(key))
                return head.value;
            head = head.next;
        }
 
        
        return null;
	}
	public boolean delete(K key) {
		
        int i = getIndex(key);
 
       
        HashNode<K,V> head = array.get(i);
 
        
        HashNode <K,V> prev = null;
        while (head != null)
        {
           
            if (head.key.equals(key))
                break;
 
            
            prev = head;
            head = head.next;
        }
 
       
        if (head == null) {
            return false;
        }
        
        size--;
 
        
        if (prev != null)
            prev.next = head.next;
        else
           array.set(i, head.next);
 
        return true;
	}
	public int size() {
		return size;
	}

	public int getIndex( K key) {
		
		 int hashCode = key.hashCode();
	     int index = hashCode % m;
	     return index;
		
		
	}
	 public boolean isEmpty() { 
		 return size == 0; 
		 }
	 public void h() {
		 LPHashTable<Integer,VOTrip> n = new LPHashTable<Integer,VOTrip>(3);
	 }
	 
	 public Iterable<K> llaves() {
	        Queue<K> cola = new Queue<K>();
	        for (int i = 0; i < array.size(); i++) 
			{
				
				cola.enqueue(array.get(i).darLlave());
				
				
			}
	        return cola;
	    } 
}
