package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements Iterable<T>
{
	private NodoListaDoblementeEncadenada<T> primero;
	private NodoListaDoblementeEncadenada<T> ultimo;
	private int size;



	public Queue() 
	{

		primero = null;
		ultimo = null;
		size = 0;
	}






	public void enqueue(T item) 
	{
		NodoListaDoblementeEncadenada<T> nodo = new NodoListaDoblementeEncadenada<T>(item);
		if (size == 0) 
		{
			primero = nodo;
			ultimo = nodo;
			size++;
		}
		else 
		{
			NodoListaDoblementeEncadenada<T> ultimotemp = ultimo;
			ultimotemp.cambiarAnterior(nodo);
			nodo.cambiarSiguiente(ultimotemp);
			ultimo = nodo;
			size++;
		}
	}


	public T getElement() 
	{
		if(isEmpty()) 
		{
			return null;
		}
		else {
			return primero.darElemento();
		}
	}



	public T dequeue() 
	{
		T elemento = null;
		if(size == 1) 
		{
			elemento = primero.darElemento();
			primero = null;
			ultimo = null;
			size--;

		}
		else if (size > 0) 
		{
			elemento = primero.darElemento();
			NodoListaDoblementeEncadenada<T> temp = primero.darAnterior();
			temp.cambiarSiguiente(null);
			primero = temp;
			size--;

		}


		return elemento;
	}

	public boolean isEmpty() 
	{
		return size == 0;
	}
	public int getSize()
	{
		return size;
	}




	class iterador implements Iterator<T>{


		public boolean hasNext() {
			return isEmpty();
		}

		public T next() {
			return dequeue();
		}
	}


	@Override
	public Iterator<T> iterator() {

		return new iterador() {
		};
	}

}
