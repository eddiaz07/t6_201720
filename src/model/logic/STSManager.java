package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

import model.vo.VOBlock;
import model.vo.VODatoParada;
import model.vo.VOParada;
import model.vo.VOStopTime;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.HashTable;
import model.data_structures.IList;
import model.data_structures.LPHashTable;
import model.data_structures.PriorityQueue;
import model.data_structures.SeparateChainingHashTable;


public class STSManager
{
	

	private LPHashTable<Integer, VOTrip> viajes;
	private HashTable<Integer, VOParada> paradas;
	private SeparateChainingHashTable<Integer, VOStopTime> stopTimes;
	private  DoubleLinkedList<VOStopTime> stoptimes;



	public STSManager() 
	{
		viajes = new LPHashTable<Integer, VOTrip>(3001);
		paradas = new HashTable<Integer, VOParada>();
		
		stoptimes = new DoubleLinkedList<VOStopTime>();
		


	}

	
	
	public LPHashTable<Integer,VODatoParada> requerimiento1(int parada)
	{
		Iterator<VOStopTime> iSTimes= stoptimes.iterator();
		LPHashTable<Integer,VODatoParada> r = new LPHashTable<Integer,VODatoParada>();
		while(iSTimes.hasNext()) {
			VOStopTime st= iSTimes.next();
			if(st.getTripId() == parada) 
			{
				VODatoParada d1= new VODatoParada(st.getStopId(),st.getTripId(),st.getArrivalTime());
				r.put(d1.hashCode(), d1);
			}
			
		}
		
	}

	
	
	
	
	public void loadTrips(String tripsFile) 
	{

	
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try
		{
			file= new FileReader(tripsFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String csvLineFields[] = cadena.split(",");

					int routeId = getIntFromCsvLine(csvLineFields, 0);
					int serviceId = getIntFromCsvLine(csvLineFields, 1);
					int tripId = getIntFromCsvLine(csvLineFields, 2);
					String tripHeadsign = getStringFromCsvLine(csvLineFields, 3);
					String tripShortName = getStringFromCsvLine(csvLineFields, 4);
					int directionId = getIntFromCsvLine(csvLineFields, 5);
					int blockId = getIntFromCsvLine(csvLineFields, 6);
					int shapeId = getIntFromCsvLine(csvLineFields, 7);
					int wheelchairAccessible = getIntFromCsvLine(csvLineFields, 8);
					int bikesAllowed = getIntFromCsvLine(csvLineFields, 9);

					VOTrip newTrip = new VOTrip(routeId, serviceId, tripId, tripHeadsign, tripShortName, directionId, blockId, shapeId, wheelchairAccessible, bikesAllowed);

					viajes.put(newTrip.hashCode(), newTrip);
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
		
		
	}
	
	
	public void loadStopTimes(String stopTimesFile) {

		DoubleLinkedList<VOStopTime> stopTime = new DoubleLinkedList<VOStopTime>();
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;
		String datos[];

		try
		{
			file= new FileReader(stopTimesFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					datos = cadena.split(",");

					int tripId = Integer.parseInt(datos[0]);
					String arrivalTime = getStringFromCsvLine(datos, 1);
					String departureTime = getStringFromCsvLine(datos, 2);
					int stopId = getIntFromCsvLine(datos, 3);
					int stopSequence = getIntFromCsvLine(datos, 4);
					Integer stopHeadsign = getNullableIntFromCsvLine(datos, 5);
					int pickupType = getIntFromCsvLine(datos, 6);
					int dropOffType = getIntFromCsvLine(datos, 7);
					Double shapeDistTraveled = null;
					if(datos.length > 8)
					{
						shapeDistTraveled = getStringFromCsvLine(datos, 8).isEmpty() ? null : Double.parseDouble(getStringFromCsvLine(datos, 8));
					}

					VOStopTime newStopTime = new VOStopTime(tripId, arrivalTime, departureTime, stopId, stopSequence, stopHeadsign, pickupType, dropOffType, shapeDistTraveled);


					stopTimes.put(newStopTime.hashCode(), newStopTime); 
					stopTime.add(newStopTime);// al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
		
	}

	

	public void loadStops(String stopsFile) {
		String cadena;

	

		FileReader file = null;
		BufferedReader reader = null;
		String datos[] = null;

		try
		{
			file= new FileReader(stopsFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					datos = cadena.split(",");

					int stopId = getIntFromCsvLine(datos, 0);
					Integer stopCode = getNullableIntFromCsvLine(datos, 1);
					String stopName = getStringFromCsvLine(datos, 2);
					String stopDesc = getStringFromCsvLine(datos, 3);
					double stopLat = Double.parseDouble(getStringFromCsvLine(datos, 4));
					double stopLon = Double.parseDouble(getStringFromCsvLine(datos, 5));
					String zoneId = getStringFromCsvLine(datos, 6);
					String stopUrl = getStringFromCsvLine(datos, 7);
					Integer locationType = getNullableIntFromCsvLine(datos, 8);
					String parentStation = "";
					if(datos.length > 9)
					{
						parentStation = getStringFromCsvLine(datos, 9);
					}

					VOParada newStop = new VOParada(stopId, stopCode, stopName, stopDesc, stopLat, stopLon, zoneId, stopUrl, locationType, parentStation);


					paradas.put(newStop.hashCode(), newStop);
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
		
	}

	
	private String getStringFromCsvLine(String[] csvLineFields, int index) {
		return csvLineFields[index].trim();
	}

	private int getIntFromCsvLine(String[] csvLineFields, int index) {
		return Integer.parseInt(getStringFromCsvLine(csvLineFields, index));
	}

	private Integer getNullableIntFromCsvLine(String[] csvLineFields, int index) {
		int number = 0;
		try 
		{
			number = getStringFromCsvLine(csvLineFields, index).isEmpty() ? null : getIntFromCsvLine(csvLineFields, index);
		} catch (Exception e) {

		}
		return number;
	}






	
	public SeparateChainingHashTable<Integer,VODatoParada> darTablaParadasPorEstaciones(int id1, int id2) throws Exception {
		DoubleLinkedList<VOStopTime> uno = new DoubleLinkedList<>();
		DoubleLinkedList<VOStopTime> dos = new DoubleLinkedList<>();
		DoubleLinkedList<VOParada> temp = new DoubleLinkedList<>();
		SeparateChainingHashTable<Integer,VODatoParada> r = new SeparateChainingHashTable<Integer,VODatoParada>();
		
		Iterator<VOStopTime> iSTimes= stoptimes.iterator();
		while(iSTimes.hasNext()) {
			VOStopTime st= iSTimes.next();
			if(st.getTripId()==id1) {
				uno.add(st);
			}
			else if(st.getTripId()==id2) {
				dos.add(st);
			}
		}
		Iterator<VOStopTime> iUno= uno.iterator();
		while(iUno.hasNext()) {
			VOStopTime st1=iUno.next();
			Iterator<VOStopTime> iDos= dos.iterator();
			while(iDos.hasNext()) {
				VOStopTime st2=iDos.next();
				if(st1.getStopId()==st2.getStopId()) {
					VODatoParada d1= new VODatoParada(st1.getStopId(),st1.getTripId(),st1.getArrivalTime());
					VODatoParada d2= new VODatoParada(st2.getStopId(),st2.getTripId(),st2.getArrivalTime());
					r.put(st1.getStopId(), d1);
					r.put(st2.getStopId(), d2);
				}
		}
			
			
	}

		return r;

}
}
