package model.vo;

public class VODatoParada implements Comparable<VODatoParada>{

	int idParada;
	int idViaje;
	String programado;
	
	public VODatoParada(int a, int b, String c) {
		this.idParada=a;
		this.idViaje=b;
		this.programado=c;
	}
	
	@Override
	public int compareTo(VODatoParada o) {
		return toString().compareTo(o.toString());
	}
	public String toString(){
		return idParada + "_" + programado + "_" + idViaje;
	}
	
}
