package model.vo;

/**
 * Representation of a route object
 */
public class VOTrip implements Comparable<VOTrip>{

    // -----------------------------------------------------------------
    // Attributes
    // -----------------------------------------------------------------

    private int routeId;
    private int serviceId;
    private int tripId;
    private String tripHeadsign;
    private String tripShortName;
    private int directionId;
    private int blockId;
    private int shapeId;
    private int wheelchairAccessible;
    private int bikesAllowed;

    // -----------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------

    /**
     * @return id - Route's id number
     */
    public VOTrip( int pRouteId, int pServiceId, int pTripId, String pTripHeadsign, String pTripShortName, int pDirectionId, int pBlockId, int pShapeId, int pWheelchairAccessible, int pBikesAllowed)
    {
        routeId = pRouteId;
        serviceId = pServiceId;
        tripId = pTripId;
        tripHeadsign = pTripHeadsign;
        tripShortName = pTripShortName;
        directionId = pDirectionId;
        blockId = pBlockId;
        shapeId = pShapeId ;
        wheelchairAccessible = pWheelchairAccessible;
        bikesAllowed = pBikesAllowed;
    }

    // -----------------------------------------------------------------
    // Methods - Getters
    // -----------------------------------------------------------------
    
    /**
     * @return name - route name
     */
    public String getName() {

        return tripShortName;
    }

    public  int id() {

        return tripId;
    }

    public int getRouteId()
    {
        return routeId;
    }

    public int getServiceId()
    {
        return serviceId;
    }

    public int getTripId()
    {
        return tripId;
    }

    public String getTripHeadsign()
    {
        return tripHeadsign;
    }

    public String getTripShortName()
    {
        return tripShortName;
    }

    public int getDirectionId()
    {
        return directionId;
    }

    public int getBlockId()
    {
        return blockId;
    }

    public int getShapeId()
    {
        return shapeId;
    }

    public int getWheelchairAccessible()
    {
        return wheelchairAccessible;
    }

    public int getBikesAllowed()
    {
        return bikesAllowed;
    }

    // -----------------------------------------------------------------
    // Methods - Setters
    // -----------------------------------------------------------------

    public void setRouteId(int newValue)
    {
        routeId = newValue;
    }

    public void setServiceId(int newValue)
    {
        serviceId = newValue;
    }

    public void setTripId(int newValue)
    {
        tripId = newValue;
    }

    public void setTripHeadsign(String newValue)
    {
        tripHeadsign = newValue;
    }

    public void setTripShortName(String newValue)
    {
        tripShortName = newValue;
    }

    public void setDirectionId(int newValue)
    {
        directionId = newValue;
    }

    public void setBlockId(int newValue)
    {
        blockId = newValue;
    }

    public void setShapeId(int newValue)
    {
        shapeId = newValue;
    }

    public void setWheelchairAccessible(int newValue)
    {
        wheelchairAccessible = newValue;
    }

    public void setBikesAllowed(int newValue)
    {
        bikesAllowed = newValue;
    }

    // -----------------------------------------------------------------
    // Methods - Logic
    // -----------------------------------------------------------------

    public int compareTo(VOTrip n) {
        int r;

        if(n.getBlockId() == blockId) {
            r=0;
        }
        else if(n.getBlockId() < blockId) {
            r=1;
        }
        else {
            r = -1;
        }

        return r;
    }
    
    public int hashCode() {
			return tripId;
	}
}
