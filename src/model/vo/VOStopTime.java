package model.vo;

/**
 * Representation of a route object
 */
public class VOStopTime implements Comparable<VOStopTime>{

    // -----------------------------------------------------------------
    // Attributes
    // -----------------------------------------------------------------

    private int tripId;
    private String arrivalTime;
    private String departureTime;
    private int stopId;
    private int stopSequence;
    private Integer stopHeadsign;
    private int pickupType;
    private int dropOffType;
    private Double shapeDistTraveled;

    // -----------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------

    /**
     * @return id - Route's id number
     */
    public VOStopTime(int pTripId, String pArrivalTime, String pDeparture_time, int pStopId, int pStopSequence, Integer pStopHeadsign,  int pPickupType, int pDropOffType, Double pShapeDistTraveled) {

        tripId = pTripId;
        arrivalTime = pArrivalTime;
        departureTime = pDeparture_time;
        stopId = pStopId;
        stopSequence = pStopSequence;
        stopHeadsign = pStopHeadsign;
        pickupType = pPickupType;
        dropOffType = pDropOffType;
        shapeDistTraveled = pShapeDistTraveled;
    }

    // -----------------------------------------------------------------
    // Methods - Getters and Setters
    // -----------------------------------------------------------------

    public String id() {

        return tripId + "_"+ arrivalTime + "_"+ stopId;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public int getStopId() {
        return stopId;
    }

    public void setStopId(int stopId) {
        this.stopId = stopId;
    }

    public int getStopSequence() {
        return stopSequence;
    }

    public void setStopSequence(int stopSequence) {
        this.stopSequence = stopSequence;
    }

    public Integer getStopHeadsign() {
        return stopHeadsign;
    }

    public void setStopHeadsign(Integer stopHeadsign) {
        this.stopHeadsign = stopHeadsign;
    }

    public int getPickupType() {
        return pickupType;
    }

    public void setPickupType(int pickupType) {
        this.pickupType = pickupType;
    }

    public int getDropOffType() {
        return dropOffType;
    }

    public void setDropOffType(int dropOffType) {
        this.dropOffType = dropOffType;
    }

    public Double getShapeDistTraveled() {
        return shapeDistTraveled;
    }

    public void setShapeDistTraveled(Double shapeDistTraveled) {
        this.shapeDistTraveled = shapeDistTraveled;
    }

    // -----------------------------------------------------------------
    // Methods - Logic
    // -----------------------------------------------------------------

    public int compareTo(VOStopTime n) {
        return this.id().compareTo(n.id());
    }
    
    public int hashCode() {
		// TODO Auto-generated method stub
		return  tripId ;
	}
}
