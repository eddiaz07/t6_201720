package model.vo;

import java.util.Iterator;


import model.data_structures.DoubleLinkedList;
import model.data_structures.PriorityQueue;

public class VOBlock implements Comparable<VOBlock> {

	
	private int id;
	private int idRuta;
	private int totalBlock;
	private DoubleLinkedList<Integer> trips;
	
	
	
	public VOBlock(int pId, int pIdRuta) {
		id=pId;
		idRuta=pIdRuta;
		totalBlock=0;
		trips= new DoubleLinkedList<Integer>();
	}
	
	public int getId() {
		return id;
	}
	public int getIdRuta() {
		return idRuta;
	}
	public int getTotalBlock() {
		return totalBlock;
	}
	public DoubleLinkedList<Integer> getTrips(){
		return trips;
	}
	public void addTrip(VOTrip p) throws Exception {
		trips.add(p.getTripId());
	}
	
	
	
	public void doHeapSort() 
	{
		DoubleLinkedList<Integer> lista = new DoubleLinkedList<Integer>();
		PriorityQueue<Integer> cola = new PriorityQueue<Integer>();
		Iterator<Integer> iterador= trips.iterator();
		while(iterador.hasNext()) 
		{
			Integer elemento = iterador.next();
			cola.enqueue(elemento);	
		}
		
		
		while(!cola.isEmpty()) 
		{
			lista.addAtEnd(cola.dequeueElementoMayor());
			
		}
		
		trips = lista;
		
	}
	
	
	
	
	
	@Override
	public int compareTo(VOBlock o) {
		if(o.getTotalBlock()==totalBlock) {
			return 0;
		}
		else if(totalBlock > o.getTotalBlock()) {
			return 1;
		}
		else {
		
		return -1;}
	}

}
