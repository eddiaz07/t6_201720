package test;

import junit.framework.TestCase;
import model.data_structures.PriorityQueue;

public class PriorityQueueTest extends TestCase {

	PriorityQueue<Integer> cola;
	protected void setUp() throws Exception {
		cola = new PriorityQueue<Integer>();
	}

	public void testPriorityQueue() 
	{
		cola.enqueue(34);
		cola.enqueue(6);
		cola.enqueue(12);
		cola.enqueue(89);
		cola.enqueue(32);
		Integer i = 89;
		assertEquals("Se cargaron los elementos del archivo Trips", cola.getSize(), 5);
		assertEquals("Se cargaron los elementos del archivo Trips", cola.dequeueElementoMayor(), i);
		assertEquals("Se cargaron los elementos del archivo Trips", cola.isEmpty(), false);
		 i = 34;
		assertEquals("Se cargaron los elementos del archivo Trips", cola.dequeueElementoMayor(), i);
		i = 32;
		assertEquals("Se cargaron los elementos del archivo Trips", cola.dequeueElementoMayor(), i);
		i = 12;
		assertEquals("Se cargaron los elementos del archivo Trips", cola.dequeueElementoMayor(), i);
		i = 6;
		assertEquals("Se cargaron los elementos del archivo Trips", cola.dequeueElementoMayor(), i);
		assertEquals("Se cargaron los elementos del archivo Trips", cola.getSize(), 0);
		assertEquals("Se cargaron los elementos del archivo Trips", cola.isEmpty(), true);
		
	}

	

}
